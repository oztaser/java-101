
import java.util.Scanner;

public class SicaklikCevirici {

	private static Scanner giris;

	static double CtoF(double sicaklik){
		return (sicaklik * 1.8) + 32;
	}
	
	static double FtoC(double sicaklik){
		return (sicaklik - 32) / 1.8;
	}
	
	public static void main(String[] args) {
		giris = new Scanner(System.in);
	
		System.out.print("1- C to F \n2- F to C\nSeçimin(1/2): ");
		
		int secim = giris.nextInt();
		
		if(secim != 1 && secim != 2)
			System.out.println("Hatalı seçim");
		else{
			
			System.out.print("Sıcaklığı gir: ");
			double sicaklik = giris.nextDouble();
			
			if(secim == 1)
			{
				System.out.println(CtoF(sicaklik));
			}
			else if(secim == 2)
			{
				System.out.println(FtoC(sicaklik));
			}
		}
	}	
}
