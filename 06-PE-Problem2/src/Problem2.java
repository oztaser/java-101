
public class Problem2 {

	
	public static int fibo(int ustLimit){
		int sonuc	= 2;
		int rakam1	= 1;
		int rakam2	= 2;
		
		while(rakam1 + rakam2 < ustLimit){
			int toplam = rakam1 + rakam2;
			rakam1 = rakam2;
			rakam2 = toplam;
			if(rakam2 % 2 == 0)
				sonuc += rakam2;
		}
		
		return sonuc;
		
	}
	
	public static void main(String[] args) {
		System.out.print(fibo(4000000));
	}
	

}
