
public class MatrisCarpimi {
	
	public static void main(String[] args) {
		int[][] matris1 		= new int [][] {{1,2},{3,4},{5,6}}; //3 x 2 
		int[][] matris2			= new int [][] {{1,2,3,4},{5,6,7,8}};// 2 x 4
		int[][]	carpimMatris 	= new int[3][4];
		
		/*
			Sonuç:
			1	2	3	4	
			3	6	9	12	
			5	10	15	20	
		 
		 */
		
		for(int i = 0; i <= 2; i++){
			for(int j = 0; j <= 1; j++)
				System.out.print(matris1[i][j]+"\t");
			System.out.println();
		}
		
		System.out.println("----------------------------------------------------");
		
		for(int k = 0; k <= 1; k++)
		{
			for(int l = 0; l <= 3; l++)
				System.out.print(matris2[k][l]+"\t");
			System.out.println();
		}
		
		for(int m = 0; m <= 3; m++){
			for(int n = 0; n  <= 2; n++){
				for(int a = 0; a < 1; a++ )
					carpimMatris[n][m] = carpimMatris[n][m] + matris1[n][a]*matris2[a][m];
			}
		}
		
		System.out.println("----------------------------------------------------");
		
		for(int b = 0; b <= 2; b++){
			for(int c = 0; c <= 3; c++)
				System.out.print(carpimMatris[b][c]+"\t");
			System.out.println();	
		}
    }

}
