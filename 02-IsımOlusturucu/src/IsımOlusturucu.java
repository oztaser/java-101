import java.util.Random;

public class IsımOlusturucu {
	
	static String  Olustur()
	{
		String ad[]		= {"ali","veli","hasan","hüseyin"};
		String soyad[]	= {"a","b","c"};
		
		Random rand = new Random();
		String ad_soyad = ad[rand.nextInt(3)] + " " + soyad[rand.nextInt(2)];
		return ad_soyad;
	}

	public static void main(String[] args) {
		for(int i = 0; i < 3;i++)
		{
			System.out.println(Olustur());
		}
	}

}
