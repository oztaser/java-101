import java.util.Scanner;

public class MetniVeKelimeleriTersCevirme {

	private static Scanner giris;
	
	public static String cevir(String metin)
	{
		String cevrilmisMetin = "";
		
		for(int i = metin.length() -1; i >= 0; i--)
		{
			cevrilmisMetin += metin.charAt(i);
		}
		
		return cevrilmisMetin;
	}
	
	public static void main(String[] args) {
		giris = new Scanner(System.in);
		
		System.out.print("Metni Gir: ");
		System.out.println(cevir(giris.nextLine()));
	}

}
