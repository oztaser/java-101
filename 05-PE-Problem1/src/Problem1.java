
public class Problem1 {

	public static void main(String[] args) {
		//1000 den küçük 3'e veya 5'e tam bölünen sayıların toplamı
		//https://projecteuler.net/problem=1  #233168
		
		int toplam = 0;
		int i = 0;
		
		while(i < 1000){
			if(i % 3 == 0 || i % 5 == 0)
				toplam = toplam + i;
			i = i + 1;
		}
		
		System.out.print(toplam);
	}

}
